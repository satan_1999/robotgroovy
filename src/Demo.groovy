package au.com.bgl

class Demo {
    static main(args) {
        def robot = new ToyRobot()
        def commands = [
            "PLACE 0,0,NORTH",
            'MOVE',
            'REPORT'
        ]
        commands.each{robot.executeRobotCommand(it)}
        robot = new ToyRobot()
        commands = ["PLACE 0,0,NORTH", 'LEFT', 'REPORT']
        commands.each{robot.executeRobotCommand(it)}
        robot = new ToyRobot()
        commands = [
            'REPORT',
            "PLACE 1,2,EAST",
            'REPORT',
            'MOVE',
            'MOVE',
            'LEFT',
            'REPORT',
            'MOVE',
            'REPORT'
        ]
        commands.each{robot.executeRobotCommand(it)}
    }
}
package au.com.bgl
@Grab(group='org.springframework', module='spring-core', version='3.2.5.RELEASE')
import org.springframework.util.Assert

class ToyRobot {
    static final int MAX_POSITION = 5
    int xPosition
    int yPosition
    au.com.bgl.Face face

    //Execute string command
    def executeRobotCommand(String commandStr ){
        commandStr = commandStr?.trim()?.toUpperCase()
        if(commandStr.startsWith(au.com.bgl.RobotCommand.PLACE.name())){
            //Execute place command
            place(commandStr)
        }else{
            au.com.bgl.RobotCommand cmd = commandStr  as au.com.bgl.RobotCommand
            Assert.notNull cmd, "Illegal RobotCommand >>#{commandStr}"
            if(!validateRobotPlace(xPosition, yPosition, face)){
                println "Please re-place robot first"
                return
            }
            
            //Execute other command
            this."${cmd.name().toLowerCase()}"()
        }
    }

    def place(String commandStr){
        def (xPosition, yPosition, face) = commandStr.replaceAll(au.com.bgl.RobotCommand.PLACE.name(), '')?.split(",")?.collect{ it.trim().toUpperCase()}
         if(validateRobotPlace(xPosition as int, yPosition as int, face as Face)){
            this.xPosition = xPosition as int
            this.yPosition = yPosition as int
            this.face = face as Face
            return
        }
        throw new IllegalArgumentException("Wrong place command - ${commandStr}")
    }
    //  the x/y position must be between 0 and maxPosition
    def validateRobotPlace(int xPosition, int yPosition,Face face){
        //face must be one of value in Face
        def result = (xPosition in 0..MAX_POSITION && yPosition in 0..MAX_POSITION && face in Face.values()) ? true : false
        if(!result){println "Invalid Placement/Move"}
        result
    }

    //report robot status
    def report(){
        println "Current Location X[${xPosition}] Y[${yPosition}] Face[${face}]"
        return "Current Location X[${xPosition}] Y[${yPosition}] Face[${face}]"
    }

    //turn left
    def left(){
        face = face.left()
    }

    //turn right
    def right(){
        face = face.right()
    }

    //move further
    def move(){
        switch(face){
            case  Face.EAST:
                xPosition =  validateRobotPlace(xPosition + 1, yPosition, face) ? xPosition + 1 : xPosition
                break
            case  Face.NORTH:
                yPosition = validateRobotPlace(xPosition, yPosition + 1, face) ? yPosition + 1 : yPosition
                break
            case  Face.WEST:
                xPosition = validateRobotPlace(xPosition - 1, yPosition, face) ? xPosition - 1 : xPosition
                break
            case  Face.SOUTH:
                yPosition = validateRobotPlace(xPosition, yPosition - 1, face) ? yPosition - 1 : yPosition
                break
            default:
                println "Invalid Direction"
        }
    }
}



package au.com.bgl

public enum Face {
    EAST(0), NORTH(1), WEST(2), SOUTH(3)
    int index
    Face(int index){
        this.index = index
    }
    
    def left(){
       // Months.values()[index]
       int nextInd = (this.index == 3 )? 0 :this.index + 1
       fromIndex(nextInd)
    }
    
    def right(){
        // Months.values()[index]
        int nextInd = (this.index == 0 )? 3 :this.index - 1
        fromIndex(nextInd)
     }
    
    def fromIndex(int ind){
        Face.values().find{it.index == ind}
    }
}

package au.com.bgl

public enum  RobotCommand {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT
}

package au.com.bgl
@Grab(group='org.spockframework', module='spock-core', version='1.0-groovy-2.3')
@Grab(group='cglib', module='cglib-nodep', version='3.1')
import spock.lang.*
class ToyRobotSpec extends Specification{
    def fixture

    def setup() {
        fixture = new au.com.bgl.ToyRobot()
        //service.springSecurityService =  [principal: null] as SpringSecurityService
    }

    def cleanup() {
    }
    
    void "test executeCommand, should throw IllegalArgumentException for wrong command"() {
        when:
        fixture.executeRobotCommand("WRONGCMD 0,0,NORTH")

        then:
        thrown(IllegalArgumentException)
    }

    void "test executeCommand, should call the right method"() {
        given:
        fixture = Spy(au.com.bgl.ToyRobot)
        def commands = ["PLACE 0,0,NORTH"]

        when:
        commands.each{fixture.executeRobotCommand(it)}

        then:
        notThrown(Exception)
        1 * fixture.place( _ as String) >> true
    }

    void "should move robot into 3,3,NORTH"() {
        given:
        def commands = [
            'REPORT',
            "PLACE 1,2,EAST",
            'REPORT',
            'MOVE',
            'MOVE',
            'LEFT',
            'REPORT',
            'MOVE',
            'REPORT'
        ]

        when:
        commands.each{fixture.executeRobotCommand(it)}

        then:
        notThrown(Exception)
        fixture.xPosition == 3
        fixture.yPosition == 3
        fixture.face == Face.NORTH
    }

    @Unroll
    void "test executeCommand, should not call move method if the orignal poistion of robot is invalid"() {
        given:
        fixture = Spy(au.com.bgl.ToyRobot)
        fixture.xPosition == 6
        fixture.yPosition == 6
        fixture.face == au.com.bgl.Face.NORTH

        when:
        fixture.executeRobotCommand(cmd)

        then:
        0 * fixture."$cmd"() >> true

        where:
        _ | cmd
        _ | "move"
        _ | "right"
        _ | "left"
        _ | "report"
    }

    void "test executeCommand, should throw illegalArgumentException"() {

        when:
        fixture.place("PLACE 8,8,NORTH")

        then:
        thrown(IllegalArgumentException)
    }

    @Unroll
    void "test executeCommand, should place robot into 1 ,1 North"() {
        when:
        fixture.place(cmd)

        then:
        fixture.xPosition == 1
        fixture.yPosition == 1
        fixture.face == au.com.bgl.Face.NORTH

        where:
        _ | cmd
        _ | "PLACE 1,1,NORTH"
        _ | "PLACE 1 ,1 ,NORTH "
        _ | "PLACE 1 ,1 ,north "
    }

    @Unroll
    void "test validateRobotPlace, should true"() {
        when:
        def result = fixture.validateRobotPlace(x ,  y, face)

        then:
        result

        where:
        x | y | face
        1 | 2 | Face.NORTH
        1 | 2 | Face.EAST
        5 | 5 | Face.WEST
        0 | 0 | Face.SOUTH
    }

    @Unroll
    void "test validateRobotPlace, should false"() {
        when:
        def result = fixture.validateRobotPlace(x ,  y, face)

        then:
        result == false

        where:
        x  | y | face
        -1 | 2 | Face.NORTH
        6  | 6 | Face.WEST
        0  | -1| Face.SOUTH
    }

    @Unroll
    void "test left, should turn to left"() {
        given:
        fixture.face = oldFace

        when:
        fixture.left()

        then:
        fixture.face == newFace

        where:
        oldFace    | newFace
        Face.EAST  | Face.NORTH
        Face.NORTH | Face.WEST
        Face.WEST  | Face.SOUTH
        Face.SOUTH | Face.EAST
    }

    @Unroll
    void "test left, should turn to right"() {
        given:
        fixture.face = oldFace

        when:
        fixture.right()

        then:
        fixture.face == newFace

        where:
        newFace    | oldFace
        Face.EAST  | Face.NORTH
        Face.NORTH | Face.WEST
        Face.WEST  | Face.SOUTH
        Face.SOUTH | Face.EAST
    }

    @Unroll
    void "test move, should move to the right position"() {
        given:
        fixture.xPosition = 3
        fixture.yPosition = 3
        fixture.face = face

        when:
        fixture.move()

        then:
        fixture.face == face
        fixture.xPosition == x
        fixture.yPosition == y

        where:
        x | y | face
        3 | 4 | Face.NORTH
        2 | 3 | Face.WEST
        3 | 2 | Face.SOUTH
        4 | 3 | Face.EAST
    }

    @Unroll
    void "test move, should not move #face to invalid position"() {
        given:
        fixture.xPosition = x
        fixture.yPosition = y
        fixture.face = face

        when:
        fixture.move()

        then:
        fixture.face == face
        fixture.xPosition == x
        fixture.yPosition == y

        where:
        x | y | face
        0 | 5 | Face.NORTH
        0 | 1 | Face.WEST
        3 | 0 | Face.SOUTH
        5 | 3 | Face.EAST
    }
}

package au.com.bgl
@Grab(group='org.spockframework', module='spock-core', version='1.0-groovy-2.3')
@Grab(group='cglib', module='cglib-nodep', version='3.1')
import spock.lang.*
class ToyRobotSpec extends Specification{
    def fixture

    def setup() {
        //service.springSecurityService =  [principal: null] as SpringSecurityService
    }

    def cleanup() {
    }

   
    @Unroll
    void "test left, should turn to left"() {
        given:
        fixture = oldFace

        expect:
        fixture.left()== newFace


        where:
        oldFace    | newFace
        Face.EAST  | Face.NORTH
        Face.NORTH | Face.WEST
        Face.WEST  | Face.SOUTH
        Face.SOUTH | Face.EAST
    }

    @Unroll
    void "test left, should turn to right"() {
        given:
        fixture = oldFace

        expect:
        fixture.right() == newFace

        where:
        newFace    | oldFace
        Face.EAST  | Face.NORTH
        Face.NORTH | Face.WEST
        Face.WEST  | Face.SOUTH
        Face.SOUTH | Face.EAST
    }

}
